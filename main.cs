using Godot;
using System;

public class main : Panel
{
	// Member variables here, example:
	// private int a = 2;
	// private string b = "textvar";
	const String DEFAULT_HOST = "127.0.0.1";
	const int DEFAULT_PORT = 52212;

	private int clientId = 0;

	public void _player_connected(int id) {
		if (GetTree().IsNetworkServer()) {
			outText("Client #" + id + " connected");
			RpcId(id, "clientOutText", "Hello client #" + id + ". Welocome to server");
			RpcId(id, "setClientId", id);
		}
	}

	public void _connected_ok() {
	}
	
	// callback from SceneTree, only for clients (not server)	
	public void _connected_fail() {
		outText("Couldn't connect");
		GetTree().SetNetworkPeer(null); //#remove peer
		disableButtons(false);
	}
	
	public void _server_disconnected() {
		outText("Server disconnected");
	}

	public void disableButtons(bool status=false) {
		GetNode<Button>("btn_server").SetDisabled(status);
		GetNode<Button>("btn_client").SetDisabled(status);
		GetNode<LineEdit>("input_text").Editable = status;
	}

	public override void _Ready()
	{
		GetTree().Connect("network_peer_connected",this,"_player_connected");
		GetTree().Connect("network_peer_disconnected",this,"_player_disconnected");
		GetTree().Connect("connected_to_server",this,"_connected_ok");
		GetTree().Connect("connection_failed",this,"_connected_fail");
	
		outText("Ready to Start");
	}

	private void outText(string text) {
		var textArea = GetNode<TextEdit>("view_text");
		var oldText = textArea.Text;
		textArea.Text = oldText + text + "\n";
	}
	
	public void _on_input_text_text_entered(string new_text) {
		if (new_text != "") {
			GetNode<LineEdit>("input_text").Clear();
			if (!GetTree().IsNetworkServer()) {
				Rpc("sendTextToServer", clientId, new_text);
			} else {
				var _outText = "Server say> " + new_text;
				outText(_outText);
				Rpc("clientOutText", _outText);
			}
		}
	}
	
	public void _on_btn_server_pressed() {
		var host = new NetworkedMultiplayerENet();
		host.SetCompressionMode(NetworkedMultiplayerENet.CompressionModeEnum.RangeCoder);
		var err = host.CreateServer(DEFAULT_PORT); // max: 1 peer, since it's a 2 players game
		if (err!=Error.Ok) {
			// is another server running?
			outText("Can't host, address in use.");
			return;
		}
		GetTree().SetNetworkPeer(host);
		disableButtons(true);
		outText("Starting server...");
	}

	public void _on_btn_client_pressed() {
		var ip = DEFAULT_HOST;
		if (!StringExtensions.IsValidIpAddress(ip)) {
			outText("IP address is invalid");
			return;
		}
		
		var host = new NetworkedMultiplayerENet();
		host.SetCompressionMode(NetworkedMultiplayerENet.CompressionModeEnum.RangeCoder);
		host.CreateClient(ip,DEFAULT_PORT);
		GetTree().SetNetworkPeer(host);
		
		disableButtons(true);
		
		outText("Connecting...");
	}

	[Remote]
	public void setClientId(int _client_id) {
		clientId = _client_id;
	}
	
	[Remote]
	public void sendTextToServer(int id, string text) {
		if (GetTree().IsNetworkServer()) {
			var _outText = "Client #" + id + " say> " + text;
			outText(_outText);
			Rpc("clientOutText", _outText);
		}
	}
	
	
	[Remote]
	public void clientOutText(string text) {
		if (!GetTree().IsNetworkServer()) {
			outText(text);
		}
	}

}
